import LessonLayout from "@/src/components/lms/Lesson/LessonLayout";
import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";

import module from "./module201.json";
import Docs2012 from "@/src/components/course-modules/201/Docs2012.mdx";

export default function Lesson2012() {
  const slug = "2012";
  const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

  return (
    <LessonLayout moduleNumber={201} sltId="201.2" slug={slug}>
      <LessonIntroAndVideo lessonData={lessonDetails} />
      <Docs2012 />
    </LessonLayout>
  );
}
