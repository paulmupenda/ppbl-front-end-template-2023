import LessonLayout from "@/src/components/lms/Lesson/LessonLayout";
import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";

import module from "./module204.json";
import { Box, Heading } from "@chakra-ui/react";
import FaucetList from "./components/FaucetList";
import { useContext } from "react";
import { PPBLContext } from "@/src/context/PPBLContext";

export default function FaucetAggregator() {
  const slug = "faucet-aggregator";
  const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);
  const ppblContext = useContext(PPBLContext)

  return (
    <Box>
      <Heading>PPBL Faucet Aggregator</Heading>
      {/* <pre>{JSON.stringify(ppblContext, null, 2)}</pre> */}
      <FaucetList metadataKey="161803398875" />
    </Box>
  );
}
