import ProjectLayout from "@/src/components/lms/Lesson/ProjectLayout";
import LessonIntroAndVideo from "@/src/components/lms/Lesson/LessonIntroAndVideo";

import module from "./module204.json";
import DocsStep01 from "@/src/components/course-modules/204/DocsStep01.mdx";

export default function ProjectStep01() {
  const slug = "project-step-01";
  const title = "Step 1: Compile a Parameterized Plutus Script"
  const lessonDetails = module.lessons.find((lesson) => lesson.slug === slug);

  return (
    <ProjectLayout moduleNumber={204} title={title} sltId="101.2" slug={slug}>
      <DocsStep01 />
    </ProjectLayout>
  );
}
